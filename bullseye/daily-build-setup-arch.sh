#!/bin/bash

ARCH=$1

# Pull in the core settings for all builds
. settings.sh
. daily.sh
. common.sh


# Set up the directories etc. needed for a named arch in the weekly
# build
if [ "$DRYRUN"x != ""x ]; then
    echo "Dry-run $0: ARCH $ARCH"
    exit 0
fi

for OUT_DIR in $OUT_FREE_SID $OUT_FW_SID; do # $OUT_FREE_TST $OUT_FW_TST 
    mkdir -p ${OUT_DIR}/${ARCH}/iso-cd ${OUT_DIR}/${ARCH}/jigdo-cd
done
