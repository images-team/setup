#!/bin/bash

# Pull in the core settings for all builds
. settings.sh
. weekly.sh
. common.sh

if [ "$DRYRUN"x != ""x ]; then
    echo "Dry-run $0:"
    if [ $DRYRUN != count ]; then
	sleep $DRYRUN_WAIT
    fi
    exit 0
fi

echo "git update debian-cd"
cd debian-cd && git pull ; cd ..

mkdir -p ${PUBDIRJIG}/trace
mkdir -p $RSYNC_TARGET/trace $RSYNC_TARGET_FIRMWARE/trace

# Keep track of the serial for the archive we're building
# against, for later archive diffing for release announce
# preparation:
serial=$(get_archive_serial)
echo "$serial" > $RSYNC_TARGET/trace/archive-serial
echo "$serial" > $RSYNC_TARGET_FIRMWARE/trace/archive-serial

#    echo "NOT checking for popcon updates"
if [ "$NOPOPCON"x = ""x ] ; then
    cd debian-cd && ./tools/update_popcon tasks/${CODENAME}/popularity-contest
    error=$?
    if [ $error != 0 ]; then
	echo "popcon update failed, error $error";
	echo "aborting";
	exit 1
    fi
    cd ..
fi

