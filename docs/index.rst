This is a setup!
================

The ``setup`` repository is the main entry point to build images. It
features a directory per codename, ensuring we can build full releases
for ``stable`` and ``oldstable``, and d-i releases for ``testing``.

Contents:

.. toctree::
    :maxdepth: 1

    release_process
