#!/bin/bash
#
# publish_from_casulana
#
# Script to receive directory tress of data from casulana, prepare
# them and publish them on pettersson* (and maybe other machines in
# future)
#
# Designed to be run via ssh keys

# Definitions
INC="/mnt/nfs-cdimage/.incoming"
WORK="/mnt/nfs-cdimage/.work"
OUT="/mnt/nfs-cdimage"
WB="$OUT/weekly-builds/"
NF_WB="$OUT/unofficial/non-free/images-including-firmware/weekly-builds/"
MIRROR="/srv/mirrors/debian/"
PARALLEL_ISO=4

. /home/debian-cd/build/common.sh

reboot_lock

VERBOSE=0
vecho ()
{
    if [ $VERBOSE -gt 0 ] ; then
	echo "$@"
    fi
}

if [ "$1"x = "-v"x ]; then
    VERBOSE=$(($VERBOSE + 1))
    shift
fi

# Iterate through the list of directories we need to work on. Move
# from the incoming directory to a working directory to avoid clashes
# with parallel runs. Once a working tree is finished, copy into place.
while read DIRS; do
    for DIR in $DIRS; do
	NUM_ISOS_BUILT=0
	if [ -d ${INC}/${DIR} ]; then
	    # Move to a temp directory to work in
	    WORKTMP=$(mktemp -u -d -p ${WORK} $(basename ${DIR})-XXXXXX)
	    mv ${INC}/${DIR} ${WORKTMP}

	    if [ -f ${WORKTMP}/.iso-wanted ]; then
		cd ${WORKTMP}
		cat .iso-wanted | xargs ~/bin/build_iso -m ${MIRROR} -j $PARALLEL_ISO
		ERROR=$?
		if [ $ERROR -eq 0 ]; then
		    NUM_ISOS_BUILT=$(cat .iso-wanted | wc -l)
		else
		    echo "Failed to build ISOs; ABORT!"
		    exit $ERROR
		fi
		cd ..
		rm -f ${WORKTMP}/.iso-wanted
	    fi
	    echo "${DIR} scanned, $NUM_ISOS_BUILT ISOs rebuilt from jigdo files"

	    # Push into place. Special handling for various locations
	    case ${DIR} in
		*-failed)
		    ARCH=$(basename $DIR "-failed")
		    case ${DIR} in
			*firmware*)
			    rsync -a $WORKTMP/ ${NF_WB}/$ARCH/
			    ;;
			*)
			    rsync -a $WORKTMP/ ${WB}/$ARCH/
			    ;;
		    esac
		    rm -rf ${WORKTMP}
		    exit 0
		    ;;
		cd-sources)
		    # cd-sources is a tree we add to over time, rather
		    # than a tree we replace as we go.
		    cp -a ${WORKTMP}/. ${OUT}/${DIR}/.
		    rm -rf ${WORKTMP}
		    ;;
		unofficial/non-free/firmware/*)
		    # We add the new dated firmware build into the
		    # right place, then maybe remove some older ones
		    cp -a ${WORKTMP}/. ${OUT}/${DIR}
		    rm -rf ${WORKTMP}
		    RELEASE=$(echo ${DIR} | awk -F / '{print $4}')
		    DATE=$(echo ${DIR} | awk -F / '{print $5}')
		    cd $OUT/unofficial/non-free/firmware/${RELEASE}
		    rm -f current
		    ln -svf $DATE current
		    echo "Removing old firmware directories:"
		    NUM=`ls -d 20*/ 2>/dev/null |wc -l`
		    NUM=$(($NUM-6))
		    if [ "$NUM" -gt "0" ] ; then
			REMOVE=`ls -1d 20* 2>/dev/null|head -n $NUM`
			echo "  $REMOVE"
			rm -rf $REMOVE
		    fi
		    ;;
		snapshots)
		    # Update the snapshots for our weekly builds
		    ~/bin/weekly-snapshots
		    rmdir ${WORKTMP}
		    ;;
	        daily-builds/*|unofficial*daily-builds/*)
		    # Build from jigdo, then do cleanup
		    if [ -d ${WORKTMP} ]; then				 
			if [ -e ${OUT}/${DIR} ]; then
			    mv ${OUT}/${DIR} ${OUT}/${DIR}.old
			fi
			mkdir -pv $(dirname ${OUT}/${DIR})
			mv ${WORKTMP} ${OUT}/${DIR}
			rm -rf ${OUT}/${DIR}.old
		    fi
		    ~/bin/remove_extra_dailies
		    ;;
		.*_release/openstack)
		    if [ -d ${WORKTMP} ]; then
			mkdir -pv $(dirname ${OUT}/${DIR})
			if [ -e ${OUT}/${DIR} ]; then
			    mv ${OUT}/${DIR} ${OUT}/${DIR}.old
			fi
			mv ${WORKTMP} ${OUT}/${DIR}
			rm -rf ${OUT}/${DIR}.old
		    fi
		    cd $OUT/$DIR/..
		    ~/live/bin/update-openstack-changelog
		    ;;
		*)
		    if [ -d ${WORKTMP} ]; then
			if [ -e ${OUT}/${DIR} ]; then
			    mv ${OUT}/${DIR} ${OUT}/${DIR}.old
			fi
			mkdir -pv $(dirname ${OUT}/${DIR})
			mv ${WORKTMP} ${OUT}/${DIR}
			rm -rf ${OUT}/${DIR}.old
		    fi
		    ;;
	    esac
	    # Make sure the output is readable; somehow we're losing
	    # readable bits somewhere in the process :-(
	    if [ -d ${OUT}/${DIR} ]; then
		find ${OUT}/${DIR} -type d | xargs chmod 755
		find ${OUT}/${DIR} -type f | xargs chmod 644
	    fi
	else
	    echo "Ignoring non-existent ${DIR}"
	fi
    done
done
