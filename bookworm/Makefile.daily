#!/usr/bin/make -f

# All the targets - finalised groups of architecture builds.
all: F_amd64 F_i386 \
     F_arm64 F_armhf F_armel \
     F_ppc64el F_mipsel F_mips64el F_s390x

export BUILDROOT=$(shell pwd)

# T_<arch>
# List all the Targets for each arch for the main weekly build.
# 
# Ordering of the component names is important - config is built up
# left-to-right. In all cases, list the components in order as:
# ARCH_DISKTYPE_option1_option2_...optionN
#
# There is an option to over-ride that config *last* using a config
# named to match the full build name with, including the leading B_
# and a trailing ".cfg", e.g. "B_i386_netinst.cfg"
T_amd64: B_amd64_netinst B_amd64_netinst_edu
T_i386: B_i386_netinst B_i386_netinst_edu
T_arm64: B_arm64_netinst
T_armhf: B_armhf_netinst
T_armel: B_armel_netinst
T_ppc64el: B_ppc64el_netinst
T_mipsel: B_mipsel_netinst
T_mips64el: B_mips64el_netinst
T_s390x: B_s390x_netinst

# Do all the initial setup for a given arch, run *once* before all the
# individual build targets for that arch
arch_setup_%::
	@./daily-build-setup-arch.sh $(word 3,$(subst _, ,$@))

# F_<arch>
# The Finalise targets; each depends on all the targets for a given
# arch, then does the final bits needed to pull together all those
# builds into a complete set, sync them, etc.
F_%: T_%
	@./daily-build-finalise-arch.sh \
		$(word 2,$(subst _, ,$@)) $(word 3,$(subst _, ,$@))

.SECONDEXPANSION:
B_%:  arch_setup_$$(word 2,$$(subst _, ,$$@))
	@./daily-build-wrapper.sh $@


.PHONY: B_*
.PHONY: T_*
