#!/bin/bash

export BUILD=$1

# Pull in the core settings for all builds
. settings.sh
. daily.sh
. common.sh

# Pull in common config based on our filename. Look for specific
# settings, based on the components of the build name. We also look
# for specific config based on the full name LAST, to allow for
# over-riding the config we've built up.
#echo "Looking for config for $BUILD"
for CFG in $(echo $BUILD | sed 's/^B_//g;s/_/ /g') $BUILD; do
    if [ -f $CFG.cfg ]; then
	CONFIG_USED="$CONFIG_USED $CFG.cfg"
#	echo "$BUILD: Including configuration from $CFG.cfg"
	. $CFG.cfg
    else
	CONFIG_UNUSED="$CONFIG_UNUSED $CFG.cfg"
#	echo "$BUILD: No config available in $CFG.cfg"
    fi
done

export CONFIG_USED
export CONFIG_UNUSED

# Sanity check - make sure we have certain variables set
check_variables ARCH CODENAME DI_CODENAME DI_DIST \
		MAXISOS MAXJIGDOS INSTALLER_CD

if [ "$DRYRUN"x != ""x ]; then
    echo "Dry-run $0: BUILD $BUILD"
    sleep $DRYRUN_WAIT
    exit 0
fi

# Execute the build step
./testingcds "$ARCH"

ret=$?
if [ $ret = 0 ]; then
    echo "  $BUILD finished successfully"
else
    echo "  $BUILD failed"
fi

exit $ret
