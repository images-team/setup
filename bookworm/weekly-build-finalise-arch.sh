#!/bin/bash

ARCH=$1

# Pull in the core settings for all builds
. settings.sh
. weekly.sh
. common.sh

# Pull together all the build output, sign, publish. etc.

if [ "$DRYRUN"x != ""x ]; then
    echo "Dry-run $0: ARCH $ARCH"
    exit 0
fi

echo "$ARCH: all individual builds complete, finalising..."

# Grab errors and the logs for this arch
for TRACE in $PUBDIRJIG/$ARCH/*.trace; do
    . $TRACE
    if [ $error -ne 0 ]; then
	arch_error="$arch_error "$BUILDNAME"FAIL/$error/$end/$logfile"
    fi
    cp log/$logfile ${PUBDIRJIG}/$ARCH/$BUILDNAME.log
done

# If we had any errors for this arch, report them. Do *not* do
# anything else - leave the last successful build in place for users
if [ "$arch_error"x != ""x ] ; then
    echo "  Reporting that $ARCH failed, arch_error $arch_error"
    ~/build.${CODENAME}/report_build_error ${PUBDIRJIG} \
	    $RSYNC_TARGET $ARCH "$arch_error"
    exit 0
fi

# No errors, so continue onwards here
if [ "$RELEASE_BUILD"x = ""x ] ; then
    echo "  $ARCH: Not generating torrent files for a non-release build"
else
    echo "  $ARCH: Generating torrent files"
    ~/build.${CODENAME}/mktorrent ${PUBDIRJIG} ${PUBDIRJIG}/$ARCH/iso-*/*.iso
fi

echo "  $ARCH: Generating checksum files"
for dir in ${PUBDIRJIG}/$ARCH/jigdo-*; do
    generate_checksums_for_arch $ARCH $dir
done

if [ "$RELEASE_BUILD"x = ""x ] ; then
    echo "  $ARCH: Signing checksums files using the automatic key"
    ~/build.${CODENAME}/sign-images ${PUBDIRJIG} $ARCH
fi

if [ "$NOSYNC"x = ""x ] ; then
    echo "  $ARCH: running iso_run ${PUBDIRJIG}/ $RSYNC_TARGET/ $ARCH"
    IRL=~/build.${CODENAME}/log/$CODENAME-$ARCH.iso_run
    START=$(now)
    ~/build.${CODENAME}/iso_run ${PUBDIRJIG}/ \
	    $RSYNC_TARGET/ $ARCH > $IRL 2>&1
    END=$(now)
    SPENT=$(calc_time $START $END)
    echo "  $ARCH: iso_run finished, took $SPENT"
fi
